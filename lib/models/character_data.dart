class CharacterData {
  int id;
  String name;
  String description;
  ThumbnailImage thumbnail;
  Series series;
  Stories stories;
  Events events;
  List<Urls> urls = new List<Urls>();
  Comics comics;

  CharacterData({
    this.id,
    this.name,
    this.description,
    this.thumbnail,
    this.series,
    this.stories,
    this.events,
    this.comics,
  });

  CharacterData.fromJson(Map<String, dynamic> jsonResponse) {
    this.id = jsonResponse['id'];
    this.name = jsonResponse['name'];
    this.description = jsonResponse['description'];
    this.thumbnail = ThumbnailImage.fromJson(jsonResponse['thumbnail']);
    this.series = Series.fromJson(jsonResponse['series']);
    this.stories = Stories.fromJson(jsonResponse['stories']);
    this.events = Events.fromJson(jsonResponse['events']);
    ((jsonResponse['urls']) as List).forEach((obj) {
      this.urls.add(Urls.fromJson(obj));
    });
    this.comics = Comics.fromJson(jsonResponse['comics']);
  }
}

class ThumbnailImage {
  String path = "";
  String extension = "";

  ThumbnailImage({this.path, this.extension});

  ThumbnailImage.fromJson(Map<String, dynamic> json) {
    this.path = json['path'];
    this.extension = json['extension'];
  }
}

class Series {
  int available;
  String collectionURI;
  int returned;

  Series({this.available, this.collectionURI, this.returned});

  Series.fromJson(Map<String, dynamic> json) {
    this.available = json['available'];
    this.collectionURI = json['collectionURI'];
    this.returned = json['returned'];
  }
}

class Comics {
  int available;
  String collectionURI;
  int returned;

  Comics({this.available, this.collectionURI, this.returned});

  Comics.fromJson(Map<String, dynamic> json) {
    this.available = json['available'];
    this.collectionURI = json['collectionURI'];
    this.returned = json['returned'];
  }
}

class Stories {
  int available;
  String collectionURI;
  int returned;

  Stories({this.available, this.collectionURI, this.returned});

  Stories.fromJson(Map<String, dynamic> json) {
    this.available = json['available'];
    this.collectionURI = json['collectionURI'];
    this.returned = json['returned'];
  }
}

class Events {
  int available;
  String collectionURI;
  int returned;

  Events({this.available, this.collectionURI, this.returned});

  Events.fromJson(Map<String, dynamic> json) {
    this.available = json['available'];
    this.collectionURI = json['collectionURI'];
    this.returned = json['returned'];
  }
}

class Urls {
  String type;
  String url;

  Urls({this.type, this.url});

  Urls.fromJson(Map<String, dynamic> json) {
    this.type = json['type'];
    this.url = json['url'];
  }
}

class ComicsData {
  String title;
  ThumbnailImage images;

  ComicsData({this.title, this.images});

  ComicsData.fromJson(Map<String, dynamic> json) {
    this.title = json['title'] ?? '';
    if (json['thumbnail'] != null)
      this.images = ThumbnailImage.fromJson(json['thumbnail']);
    else
      this.images = ThumbnailImage();
  }
}
