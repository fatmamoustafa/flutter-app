import 'package:flutter/material.dart';

class LoginModel {
  String name;
  String password;

  LoginModel({@required this.name, @required this.password});

  LoginModel.fromJson(Map<String, dynamic> json) {
    this.name = json["name"];
    this.password = json["password"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["name"] = this.name ?? "";
    data["password"] = this.password ?? "";
    return data;
  }
}
