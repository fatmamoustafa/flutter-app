import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferenceHelper {
  static SharedPreferences _prefs;

  static final SharedPreferenceHelper _instance =
      SharedPreferenceHelper._internal();

  factory SharedPreferenceHelper({SharedPreferences pre}) {
    _prefs = pre != null ? pre : _prefs;
    if (_prefs == null) {
      _initPrefs();
    }
    return _instance;
  }

  SharedPreferenceHelper._internal();

  static _initPrefs() async {
    _prefs = await SharedPreferences.getInstance();
  }

  dynamic getValueForKey(String key) {
    return _prefs?.get(key);
  }

  dynamic getValueForKeyWithAlternativeValue(
      {@required String key, @required dynamic alternativeValue}) {
    if (_prefs.get(key) == null) setValueForKey(key, alternativeValue);
    return _prefs.get(key);
  }

  setValueForKey(String key, dynamic value) {
    if (_prefs == null) {
      _initPrefs();
    }
    if (value == null) {
      removeValueForKey(key);
    } else if (value is int) {
      _prefs.setInt(key, value);
    } else if (value is String) {
      _prefs.setString(key, value);
    } else if (value is bool) {
      _prefs.setBool(key, value);
    } else {
      throw "unknown value type";
    }
  }

  Future<bool> removeValueForKey(String key) {
    return _prefs.remove(key);
  }
}
