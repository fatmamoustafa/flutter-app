import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'app_constants.dart';
import 'package:convert/convert.dart';

class Helper {

  static String getTS() {
    return DateTime.now().millisecondsSinceEpoch.toString();
  }

  static String getHASH() {
    var data = getTS() + PRIVATEKEY + APIKEY;
    var content = new Utf8Encoder().convert(data);
    var digest = md5.convert(content);
    return hex.encode(digest.bytes);
  }
}
