import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:http/http.dart' as http;

class NetworkUtils {
  static final client = new http.Client();

  static Future<bool> checkConnectivity() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi)
      return true;
    else
      return false;
  }

  static addHeaders(var req) {
    req.headers.addAll({"Accept": "application/json"});
    req.headers.addAll({"Content-Type": "application/json"});
  }
}
