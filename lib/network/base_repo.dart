import 'package:http/http.dart' as http;

import '../pages/base/request/app_api_request.dart';
import 'network_utils.dart';

class BaseRepo {
  static final BaseRepo _baseRepo = BaseRepo._internal();

  factory BaseRepo() {
    return _baseRepo;
  }

  BaseRepo._internal();

  String response;

  Future<String> createApiCall<T extends BaseApiRequest>(T request) async {
    bool connected = await NetworkUtils.checkConnectivity();
    if (connected) {
      response = await _getDataRemotely<T>(request);
      print("response$response");
    } else {
      response = "Check internet connection";
    }
    return response;
  }

  Future<String> _getDataRemotely<T extends BaseApiRequest>(T request) async {
    String returnResponse;

    var req = http.Request(request.type, Uri.parse(request.baseUrl));
    print(req);
    NetworkUtils.addHeaders(req);

    var response = await NetworkUtils.client.send(req);
    if (response.statusCode == 200) {
      returnResponse = await response.stream.bytesToString();
    } else {
      if (response.statusCode == 401) returnResponse = "Unauthorized";
    }
    return returnResponse;
  }
}
