import 'package:flutter_app/helpers/app_constants.dart';
import 'package:flutter_app/helpers/helper.dart';
import 'package:flutter_app/pages/base/request/app_api_request.dart';

class GetMoviesRequest extends BaseApiRequest {
  GetMoviesRequest() {
    baseUrl = BASEURL +
        '/characters?ts=' +
        Helper.getTS() +
        '&apikey=' +
        APIKEY +
        '&hash=' +
        Helper.getHASH();
    type = "GET";
  }
}
