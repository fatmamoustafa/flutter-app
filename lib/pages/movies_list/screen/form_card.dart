import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/models/login_model.dart';
import 'package:flutter_app/pages/base/bloc/application_bloc.dart';
import 'package:flutter_app/pages/base/bloc/base_bloc.dart';
import 'package:toast/toast.dart';

import '../../../helpers/ensure_visible.dart';
import '../../../helpers/size_config.dart';

class FormCard extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return CardState();
  }
}

class CardState extends State<FormCard> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final FocusNode nameFocusNode = FocusNode();
  final FocusNode passwordFocusNode = FocusNode();
  TextEditingController passwordController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  ApplicationBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<ApplicationBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      width: double.infinity,
      height: SizeConfig.safeBlockVertical * 57,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8.0),
          boxShadow: [
            BoxShadow(
                color: Colors.black12,
                offset: Offset(0.0, 15.0),
                blurRadius: 15.0),
            BoxShadow(
                color: Colors.black12,
                offset: Offset(0.0, -10.0),
                blurRadius: 10.0),
          ]),
      child: Padding(
        padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
        child: Form(
          key: _formKey,
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Center(
                    child: Text(
                  "Login",
                  style: TextStyle(
                      fontSize: SizeConfig.safeBlockHorizontal * 5,
                      fontFamily: "Poppins-Bold",
                      letterSpacing: .6),
                )),
                SizedBox(
                  height: SizeConfig.safeBlockVertical * 1,
                ),
                Text(
                  "Username",
                  style: TextStyle(
                    fontSize: SizeConfig.safeBlockHorizontal * 4,
                    fontFamily: "Poppins-Medium",
                  ),
                ),
                _buildUserNameFiled(),
                SizedBox(
                  height: SizeConfig.safeBlockVertical * 3,
                ),
                Text(
                  "Password",
                  style: TextStyle(
                    fontSize: SizeConfig.safeBlockHorizontal * 4,
                    fontFamily: "Poppins-Medium",
                  ),
                ),
                _buildPasswordFiled(),
                SizedBox(
                  height: SizeConfig.safeBlockVertical * 3,
                ),
                _drawLoginButton(),
                SizedBox(height: SizeConfig.safeBlockVertical * 3),
                Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "Forget password",
                        style: TextStyle(
                            color: Colors.blue,
                            fontFamily: "Poppins-Medium",
                            fontSize: SizeConfig.safeBlockHorizontal * 3),
                      ),
                    ]),
                SizedBox(height: SizeConfig.safeBlockVertical * 3),
              ]),
        ),
      ),
    );
  }

  void _showToastMessage(BuildContext context) {
    Toast.show('There is an error', context);
  }

  _buildUserNameFiled() {
    return EnsureVisibleWhenFocused(
      focusNode: nameFocusNode,
      child: TextFormField(
        controller: nameController,
        validator: (String v) {
          if (v.isEmpty) {
            return 'you should enter your name';
          }
          return "";
        },
        onSaved: (String v) {
          if (v.isNotEmpty) {
            nameController.text = v;
          }
        },
        decoration: InputDecoration(
            hintText: "Username",
            hintStyle: TextStyle(
              fontSize: SizeConfig.safeBlockHorizontal * 3,
              color: Colors.grey,
            )),
      ),
    );
  }

  _buildPasswordFiled() {
    return EnsureVisibleWhenFocused(
      focusNode: passwordFocusNode,
      child: TextFormField(
        controller: passwordController,
        focusNode: passwordFocusNode,
        obscureText: true,
        keyboardType: TextInputType.visiblePassword,
        validator: (String v) {
          if (v.isEmpty) {
            return 'you should enter your password';
          }
//          return "";
        },
        onSaved: (String v) {
          if (v.isNotEmpty) {
            passwordController.text = v;
          }
        },
        decoration: InputDecoration(
            hintText: "Password",
            hintStyle: TextStyle(
              fontSize: SizeConfig.safeBlockHorizontal * 3,
              color: Colors.grey,
            )),
      ),
    );
  }

  void _validateUserData() {
    if (nameController.text.toString().isNotEmpty &&
        passwordController.text.toString().isNotEmpty) {
      Map<String, dynamic> userdata = new Map<String, dynamic>();
      userdata["name"] = nameController.value.text.toString();
      userdata["password"] = nameController.value.text.toString();
      bloc.userDataModel(
        LoginModel.fromJson(userdata),
      );
      Navigator.pushReplacementNamed(context, '/admin');
    } else {
      _showToastMessage(context);
    }
  }

  _drawLoginButton() {
    return Center(
      child: Container(
        width: SizeConfig.safeBlockVertical * 15,
        height: SizeConfig.safeBlockHorizontal * 10,
        child: FlatButton(
          onPressed: () {
            _validateUserData();
          },
          textColor: Colors.white,
          color: Colors.transparent,
          child: Text(
            'LOG IN',
            style: TextStyle(
                fontSize: SizeConfig.safeBlockVertical * 2.5,
                fontFamily: "Poppines-Bold",
                letterSpacing: 1.0),
          ),
        ),
        decoration: BoxDecoration(
          gradient:
              LinearGradient(colors: [Color(0xFF6078EA), Color(0xFF17EAD9)]),
          borderRadius: BorderRadius.circular(6.0),
        ),
      ),
    );
  }
}
