import 'package:flutter/material.dart';
import 'package:flutter_app/models/login_model.dart';
import 'package:flutter_app/pages/base/bloc/application_bloc.dart';
import 'package:flutter_app/pages/base/bloc/base_bloc.dart';
import 'package:flutter_app/pages/base/bloc/bloc_event_state_builder.dart';
import 'package:flutter_app/pages/details_page/bloc/details_page_bloc.dart';
import 'package:flutter_app/pages/movies_list/bloc/get_movies_event.dart';
import 'package:flutter_app/pages/movies_list/bloc/movies_page_bloc.dart';
import 'package:flutter_app/pages/movies_list/bloc/movies_screen_state.dart';

import '../../../helpers/images.dart';
import '../../../helpers/size_config.dart';
import '../../../models/character_data.dart';
import '../../details_page/screen/details_page.dart';

class MoviesList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.lightBlue,
        brightness: Brightness.dark,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var characters = <CharacterData>[];
  MoviesBloc bloc;
  ApplicationBloc applicationBloc;

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<MoviesBloc>(context);
    applicationBloc = BlocProvider.of<ApplicationBloc>(context);
    bloc.emitEvent(GetDataEvent());
    LoginModel model = applicationBloc.userData;
    String v = model.toJson().toString();
    print("userData($v");
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
        appBar: AppBar(
          title: Center(
            child: Padding(
              padding: const EdgeInsets.only(left: 25),
              child: Container(
                width: SizeConfig.screenWidth * 0.25,
                height: SizeConfig.screenWidth * 0.19,
                child: Image.asset(
                  LOGO,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          actions: <Widget>[
            IconButton(
                icon: Icon(
                  Icons.search,
                  color: Colors.red,
                ),
                onPressed: () {}),
          ],
        ),
        body: Container(child: getMoviesList()));
  }

  Widget getMoviesList() {
    return BlocEventStateBuilder<MoviesScreenState>(
      bloc: bloc,
      builder: (BuildContext context, MoviesScreenState state) {
        if (state != null) {
          if (!state.dateLoaded) {
            return new Center(child: new CircularProgressIndicator());
          } else if (state.dateLoaded && state.characters != null) {
            List items = state.characters;
            return ListView.builder(
                itemCount: items.length,
                itemBuilder: (context, index) {
                  try {
                    return buildItem(items[index]);
                  } catch (exception) {
                    print(exception);
                    return Container();
                  }
                });
          } else {
            WidgetsBinding.instance.addPostFrameCallback(
                (_) => Scaffold.of(context).showSnackBar(SnackBar(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(22),
                              topRight: Radius.circular(22))),
                      content: Text(state.errorMsg),
                      duration: Duration(days: 365),
                      action: SnackBarAction(
                        label: 'Try again',
                        onPressed: () {
                          bloc.emitEvent(GetDataEvent());
                        },
                      ),
                    )));
            return Container();
          }
        } else
          return Container();
      },
    );
  }

  Widget buildItem(CharacterData it) {
    return Stack(children: <Widget>[
      GestureDetector(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => BlocProvider<DetailsPageBloc>(
                        bloc: DetailsPageBloc(),
                        child: CharacterDetailsPage(it),
                      )));
        },
        child: Image.network(
          it.thumbnail.path + "." + it.thumbnail.extension,
          width: MediaQuery.of(context).size.width,
          height: SizeConfig.safeBlockHorizontal * 55,
          fit: BoxFit.cover,
        ),
      ),
      Container(
          margin: EdgeInsets.only(top: SizeConfig.blockSizeHorizontal * 35),
          width: SizeConfig.blockSizeVertical * 35,
          height: SizeConfig.safeBlockHorizontal * 20,
          child: Center(
              child: Text(
            it.name,
            style: TextStyle(
                fontSize: SizeConfig.safeBlockHorizontal * 4,
                letterSpacing: .6,
                color: Colors.black,
                fontWeight: FontWeight.bold),
          )),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                RECTANGLE,
              ),
            ),
          ))
    ]);
  }
}
