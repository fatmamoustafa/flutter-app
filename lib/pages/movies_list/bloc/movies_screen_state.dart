import 'package:flutter_app/models/character_data.dart';
import 'package:flutter_app/pages/base/bloc/bloc_event_state_base.dart';

class MoviesScreenState extends BlocState {
  bool dateLoaded;
  List<dynamic> characters;
  String errorMsg;

  MoviesScreenState({bool loaded, List<CharacterData> characters}) {
    this.dateLoaded = loaded;
    this.characters = characters;
  }

  MoviesScreenState.load() {
    this.dateLoaded = false;
  }

  MoviesScreenState.dataLoaded(List<dynamic> characters) {
    this.dateLoaded = true;
    this.characters = characters;
  }
  MoviesScreenState.handelErrorType(String msg) {
    this.errorMsg = msg;
    this.dateLoaded = true;
  }
}
