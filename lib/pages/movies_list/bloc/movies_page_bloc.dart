import 'dart:convert';

import 'package:flutter_app/models/character_data.dart';
import 'package:flutter_app/network/base_repo.dart';
import 'package:flutter_app/pages/base/bloc/bloc_event_state_base.dart';

import '../request/movies_request.dart';
import 'get_movies_event.dart';
import 'movies_screen_state.dart';

class MoviesBloc extends BlocEventStateBase<GetDataEvent, MoviesScreenState> {
  BaseRepo _baseRepo;
  List<CharacterData> list = new List();

  MoviesBloc() {
    _baseRepo = BaseRepo();
  }

  @override
  Stream<MoviesScreenState> eventHandler(
      GetDataEvent event, MoviesScreenState currentState) async* {
    if (event is GetDataEvent) {
      yield MoviesScreenState.load();
      String response =
          await _baseRepo.createApiCall<GetMoviesRequest>(GetMoviesRequest());
      if (response != null) {
        print("response: $response");
        if (response.contains("200")) {
          List chars = json.decode(response)['data']['results'] as List;
          chars.forEach((obj) {
            this.list.add(CharacterData.fromJson(obj));
          });
          yield MoviesScreenState.dataLoaded(list);
        } else {
          yield MoviesScreenState.handelErrorType(response);
        }
      }
    }
  }
}
