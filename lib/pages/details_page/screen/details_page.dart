import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_app/helpers/app_constants.dart';
import 'package:flutter_app/helpers/helper.dart';
import 'package:flutter_app/helpers/size_config.dart';
import 'package:flutter_app/models/character_data.dart';
import 'package:flutter_app/pages/base/bloc/base_bloc.dart';
import 'package:flutter_app/pages/details_page/bloc/details_page_bloc.dart';
import 'package:flutter_web_browser/flutter_web_browser.dart';
import 'package:http/http.dart' as http;

class CharacterDetailsPage extends StatelessWidget {
  CharacterData it;

  CharacterDetailsPage(this.it);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primarySwatch: Colors.blue, brightness: Brightness.dark),
      home: DetailsPage(it),
    );
  }
}

class DetailsPage extends StatefulWidget {
  CharacterData currentCharacter;

  DetailsPage(this.currentCharacter);

  @override
  State<StatefulWidget> createState() {
    return MarvelDetailsState();
  }
}

class MarvelDetailsState extends State<DetailsPage> {
  DetailsPageBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<DetailsPageBloc>(context);
    /*bloc.emitEvent(
        GetCollectionEvent(widget.currentCharacter.comics.collectionURI));
    bloc.emitEvent(
        GetCollectionEvent(widget.currentCharacter.events.collectionURI));
    bloc.emitEvent(
        GetCollectionEvent(widget.currentCharacter.stories.collectionURI));
    bloc.emitEvent(
        GetCollectionEvent(widget.currentCharacter.series.collectionURI));*/
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text(widget.currentCharacter.name)),
      ),
      body: Stack(
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              widget.currentCharacter.thumbnail.path != ""
                  ? Expanded(
                      child: Image.network(
                          widget.currentCharacter.thumbnail.path +
                              "." +
                              widget.currentCharacter.thumbnail.extension,
                          fit: BoxFit.fill,
                          color: Color.fromRGBO(80, 80, 80, 0.8),
                          colorBlendMode: BlendMode.modulate),
                    )
                  : Container(),
            ],
          ),
          SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: SizeConfig.blockSizeHorizontal * 55,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(
                            widget.currentCharacter.thumbnail.path +
                                "." +
                                widget.currentCharacter.thumbnail.extension),
                        fit: BoxFit.fitWidth),
                  ),
                ),
                buildNameAndDescriptionWidget(
                    "Name :", widget.currentCharacter.name, 4),
                buildNameAndDescriptionWidget(
                    "Description : ", widget.currentCharacter.description, 3.5),
                buildTitleAndItsList(
                    "Comics :", widget.currentCharacter.comics.collectionURI),
                buildTitleAndItsList(
                    "Events :", widget.currentCharacter.events.collectionURI),
                buildTitleAndItsList(
                    "Stories :", widget.currentCharacter.stories.collectionURI),
                buildTitleAndItsList(
                    "Series :", widget.currentCharacter.series.collectionURI),
                buildUrls(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildNameAndDescriptionWidget(String title, String name, double size) {
    return name != null && name.isNotEmpty
        ? Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    title,
                    style: TextStyle(
                        color: Colors.redAccent,
                        fontSize: SizeConfig.blockSizeHorizontal * 4),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    name,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: SizeConfig.blockSizeHorizontal * size),
                  ),
                )
              ])
        : Container();
  }

  Widget buildTitleAndItsList(String title, String url) {
    return Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              title,
              style: TextStyle(
                  color: Colors.redAccent,
                  fontSize: SizeConfig.blockSizeHorizontal * 4),
            ),
          ),
          Container(
              height: SizeConfig.safeBlockHorizontal * 45,
              child: FutureBuilder(
                  future: getComics(url),
                  builder: (BuildContext context, AsyncSnapshot asyncSnapshot) {
                    print("asyncSnapshot $asyncSnapshot");
                    if (asyncSnapshot.hasData == false &&
                        asyncSnapshot.connectionState.toString() ==
                            "ConnectionState.waiting") {
                      return Center(child: CircularProgressIndicator());
                    } else if (asyncSnapshot.data != null &&
                        asyncSnapshot.data.length != 0) {
                      return ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: asyncSnapshot.data.length,
                        itemBuilder: (context, index) {
                          return buildImagesItem(
                              asyncSnapshot.data[index] as ComicsData, context);
                        },
                      );
                      /* BlocEventStateBuilder<DetailsPageState>(
                  bloc: bloc,
                  builder:
                      (BuildContext context, DetailsPageState stateSnapshot) {
                    if (!stateSnapshot.listLoaded) {
                      return new Center(child: new CircularProgressIndicator());
                    } else if (stateSnapshot.list.length > 0) {
                      return ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: stateSnapshot.list.length,
                        itemBuilder: (context, index) {
                          return buildImagesItem(
                              stateSnapshot.list[index], context);
                        },
                      );
                    }
                    return Container();
                  */
                    } else {
                      return Container();
                    }
                  })),
        ]);
  }

  Widget buildImagesItem(ComicsData it, BuildContext context) {
    return Stack(children: <Widget>[
      Container(
        margin: EdgeInsets.only(left: 8.0),
        width: SizeConfig.safeBlockVertical * 24,
        height: SizeConfig.safeBlockVertical * 29,
        decoration: BoxDecoration(
            color: Colors.white,
            image: DecorationImage(
                image: NetworkImage(
                  it.images.path + "." + it.images.extension,
                ),
                fit: BoxFit.fill),
            borderRadius: BorderRadius.circular(8.0),
            boxShadow: [
              BoxShadow(
                  color: Colors.black12,
                  offset: Offset(0.0, 15.0),
                  blurRadius: 15.0),
              BoxShadow(
                  color: Colors.black12,
                  offset: Offset(0.0, -10.0),
                  blurRadius: 10.0),
            ]),
      ),
      Container(
        padding: EdgeInsets.all(8.0),
        child: Center(
          child: SizedBox(
            width: SizeConfig.safeBlockHorizontal * 24,
            height: SizeConfig.safeBlockHorizontal * 23,
            child: AutoSizeText(
              it.title,
              style: TextStyle(
                  fontSize: SizeConfig.safeBlockHorizontal * 3,
                  letterSpacing: .6,
                  color: Colors.white,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ),
    ]);
  }

  Widget buildUrls() {
    return widget.currentCharacter.urls != null &&
            widget.currentCharacter.urls.isNotEmpty
        ? Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Urls:',
                    style: TextStyle(
                        color: Colors.redAccent,
                        fontSize: SizeConfig.blockSizeHorizontal * 4),
                  ),
                ),
                Container(
                  color: Colors.white,
                  height: SizeConfig.safeBlockHorizontal * 45,
                  child: ListView.builder(
                    itemCount: widget.currentCharacter.urls.length,
                    itemBuilder: (context, index) {
                      return buildUrlsItem(
                          widget.currentCharacter.urls[index] as Urls, context);
                    },
                  ),
                )
              ])
        : Container();
  }

  Widget buildUrlsItem(Urls url, BuildContext context) {
    print(url.url);
    return Column(
      children: <Widget>[
        GestureDetector(
          onTap: () {
            _launchURL(url.url);
          },
          child: Container(
            padding: EdgeInsets.all(8.0),
            decoration: BoxDecoration(color: Colors.white),
            width: SizeConfig.screenWidth,
            child: Text(url.url,
                style: TextStyle(
                    fontSize: SizeConfig.safeBlockHorizontal * 4,
                    letterSpacing: .6,
                    color: Colors.blue,
                    fontWeight: FontWeight.normal)),
          ),
        ),
        Divider(
          thickness: 1.0,
          color: Colors.black26,
        )
      ],
    );
  }

  _launchURL(String url) async {
    await FlutterWebBrowser.openWebPage(
        url: url, androidToolbarColor: Colors.deepPurple);

//    if (await canLaunch(url)) {
//      await launch(url);
//    } else {
//      throw 'Could not launch $url';
//    }
  }

  getComics(String collectionUrl) async {
    final String url = collectionUrl +
        '?ts=' +
        Helper.getTS() +
        '&apikey=' +
        APIKEY +
        '&hash=' +
        Helper.getHASH();
    try {
      http.Response streamedRest = await http
          .get(Uri.encodeFull(url), headers: {"Accept": "application/json"});
      List comics = [];
      if (streamedRest.statusCode == 200) {
        var charactersData =
            json.decode(streamedRest.body)['data']['results'] as List;
        for (var i in charactersData) {
          comics.add(
            new ComicsData(
              title: i['title'],
              images: ThumbnailImage(
                path: i['thumbnail']['path'],
                extension: i['thumbnail']['extension'],
              ),
            ),
          );
        }
        return comics;
      }
    } catch (exception) {
      print(exception);
    }
  }
}
