import 'package:flutter_app/helpers/app_constants.dart';
import 'package:flutter_app/helpers/helper.dart';
import 'package:flutter_app/pages/base/request/app_api_request.dart';

class CollectionRequest extends BaseApiRequest {
  CollectionRequest(String collectionUrl) {
    baseUrl = collectionUrl +
        '?ts=' +
        Helper.getTS() +
        '&apikey=' +
        APIKEY +
        '&hash=' +
        Helper.getHASH();
    type = "GET";
  }
}
