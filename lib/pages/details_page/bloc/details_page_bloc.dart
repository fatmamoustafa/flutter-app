import 'dart:convert';

import 'package:flutter_app/models/character_data.dart';
import 'package:flutter_app/network/base_repo.dart';
import 'package:flutter_app/pages/base/bloc/bloc_event_state_base.dart';
import 'package:flutter_app/pages/details_page/bloc/details_page_state.dart';
import 'package:flutter_app/pages/details_page/request/collection_request.dart';

import 'get_collection_event.dart';

class DetailsPageBloc
    extends BlocEventStateBase<GetCollectionEvent, DetailsPageState> {
  BaseRepo _baseRepo;

  DetailsPageBloc() {
    _baseRepo = BaseRepo();
  }

  @override
  Stream<DetailsPageState> eventHandler(
      GetCollectionEvent event, DetailsPageState currentState) async* {
    if (event is GetCollectionEvent) {
      yield DetailsPageState.load();
      String response = await _baseRepo
          .createApiCall<CollectionRequest>(CollectionRequest(event.url));
      if (response != null) {
        print("response: $response");
        if (response.contains("200")) {
          List chars = json.decode(response)['data']['results'] as List;
          List<ComicsData> list = new List();
          if (chars.length != 0)
            chars.forEach((obj) {
              list.add(ComicsData.fromJson(obj));
            });
          yield DetailsPageState.dataLoaded(list);
        } else {
          yield DetailsPageState.handelErrorType(response);
        }
      }
    }
  }
}
