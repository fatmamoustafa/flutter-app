import 'package:flutter_app/models/character_data.dart';
import 'package:flutter_app/pages/base/bloc/bloc_event_state_base.dart';

class DetailsPageState extends BlocState {
  bool listLoaded;
  List<ComicsData> list;
  String errorMsg;

  DetailsPageState(bool listLoaded, List<ComicsData> comics) {
    this.list = comics;
    this.listLoaded = listLoaded;
  }

  DetailsPageState.load() {
    this.listLoaded = false;
  }

  DetailsPageState.dataLoaded(List<ComicsData> comics) {
    this.list = comics;
    this.listLoaded = true;
  }

  DetailsPageState.handelErrorType(String response) {
    this.errorMsg = response;
    this.listLoaded = true;
  }
}
