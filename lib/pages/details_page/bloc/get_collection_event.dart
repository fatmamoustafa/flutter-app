import 'package:flutter_app/pages/base/bloc/bloc_event_state_base.dart';

class GetCollectionEvent extends BlocEvent {
  String url;

  GetCollectionEvent(String collectionUrl) {
    this.url = collectionUrl;
  }
}
