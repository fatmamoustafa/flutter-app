import 'package:flutter_app/models/login_model.dart';
import 'package:flutter_app/pages/base/bloc/bloc_event_state_base.dart';
import 'package:rxdart/rxdart.dart';

//class ApplicationBloc extends BlocBase {
class ApplicationBloc extends BlocEventStateBase {
  @override
  Stream eventHandler(event, currentState) {
    return null;
  }

  BehaviorSubject<LoginModel> dataSubject = new BehaviorSubject<LoginModel>();

  void userDataModel(LoginModel model) {
    /*SharedPrefrenceHelper().setValueForKey(
        SharedPreferencesKeys.USER_DATA, jsonEncode(model.toJson()));*/
    if (!dataSubject.isClosed) {
      dataSubject.sink.add(model);
    }
  }

  get userData {
    return dataSubject.stream.value;
  }

  @override
  void dispose() {
    dataSubject.close();
  }
}
