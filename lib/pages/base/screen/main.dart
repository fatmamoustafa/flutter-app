import 'package:flutter/material.dart';
import 'package:flutter_app/pages/base/bloc/application_bloc.dart';
import 'package:flutter_app/pages/base/bloc/base_bloc.dart';
import 'package:flutter_app/pages/login/login.dart';
import 'package:flutter_app/pages/movies_list/screen/movies_list.dart';

import '../../movies_list/bloc/movies_page_bloc.dart';

void main() => runApp(
    BlocProvider<ApplicationBloc>(bloc: ApplicationBloc(), child: MyApp()));

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        '/': (BuildContext context) => Login(),
        '/admin': (BuildContext context) => BlocProvider<MoviesBloc>(
              bloc: MoviesBloc(),
              child: MoviesList(),
            )
      },
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
//      home: Login(),
    );
  }
}
