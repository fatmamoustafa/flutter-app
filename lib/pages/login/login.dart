import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/customwidgets/custom_icons.dart';
import 'package:flutter_app/customwidgets/social_icons.dart';
import 'package:flutter_app/helpers/images.dart';
import 'package:flutter_app/helpers/size_config.dart';
import 'package:flutter_app/pages/movies_list/screen/form_card.dart';

class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new LoginState();
  }
}

class LoginState extends State<Login> {
  bool isChecked = false;
  void _radio() {
    setState(() {
      isChecked = !isChecked;
    });
  }

  Widget radioButton(bool isChecked) => new Container(
      width: 16.0,
      height: 16.0,
      padding: EdgeInsets.all(2.0),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(width: 2.0, color: Colors.black),
      ),
      child: isChecked
          ? Container(
              width: double.infinity,
              height: double.infinity,
              decoration:
                  BoxDecoration(shape: BoxShape.circle, color: Colors.black))
          : Container());

  Widget horizontalLine() => Padding(
        padding: EdgeInsets.symmetric(horizontal: 13.0),
        child: Container(
          height: 1,
          width: SizeConfig.safeBlockVertical * 11,
          color: Colors.black26.withOpacity(.2),
        ),
      );

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: false,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 15.0),
                child: Image.asset(
                  SHAKE_HANDS,
                  height: SizeConfig.blockSizeHorizontal * 50,
                ),
              ),
              Expanded(
                child: Container(),
              ),
              Image.asset(
                TOWER,
                height: SizeConfig.blockSizeHorizontal * 37,
              )
            ],
          ),
          SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(top: 50, right: 28.0, left: 28.0),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Image.asset(
                        LOGIN_LOGO,
                        width: SizeConfig.safeBlockVertical * 10,
                        height: SizeConfig.safeBlockHorizontal * 10,
                      ),
                      Text(
                        "LOGO",
                        style: TextStyle(
                            fontFamily: "Poppiens-Bold",
                            fontSize: SizeConfig.safeBlockHorizontal * 6,
                            letterSpacing: .6,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                  SizedBox(
                    height: SizeConfig.safeBlockHorizontal * 25,
                  ),
                  FormCard(),
                  SizedBox(
                    height: SizeConfig.safeBlockHorizontal * 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          SizedBox(
                            width: SizeConfig.safeBlockVertical * 1.5,
                          ),
                          GestureDetector(
                            onTap: _radio,
                            child: radioButton(isChecked),
                          ),
                          SizedBox(
                            width: SizeConfig.safeBlockVertical * 1.5,
                          ),
                          Text(
                            "Remember me",
                            style: TextStyle(
                              fontSize: SizeConfig.safeBlockVertical * 2.5,
                              fontFamily: "Poppins-Meduim",
                            ),
                          )
                        ],
                      ),
                      InkWell(
                        onTap: () {},
                        child: Container(
                          width: SizeConfig.safeBlockVertical * 20,
                          height: SizeConfig.safeBlockHorizontal * 11,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                                colors: [Color(0xFF17EAD9), Color(0xFF6078EA)]),
                            borderRadius: BorderRadius.circular(6.0),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0xFF6078EA).withOpacity(.2),
                                blurRadius: 8.0,
                                offset: Offset(0.0, 8.0),
                              )
                            ],
                          ),
                          child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                              child: Center(
                                child: Text("SIGN UP",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize:
                                            SizeConfig.safeBlockVertical * 2.7,
                                        fontFamily: "Poppines-Bold",
                                        letterSpacing: 1.0)),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: SizeConfig.safeBlockHorizontal * 3,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      horizontalLine(),
                      Text(
                        "Sotial Login",
                        style: TextStyle(
                          fontFamily: "Poppins-Bold",
                          fontSize: SizeConfig.safeBlockHorizontal * 5,
                        ),
                      ),
                      horizontalLine()
                    ],
                  ),
                  SizedBox(
                    height: SizeConfig.safeBlockHorizontal * 2,
                  ),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SocialIcons(colorsList: [
                          Color(0xFF102397),
                          Color(0xFF187ADF),
                          Color(0xFF00EEF8)
                        ], iconData: CustomIcons.facebook, onPressed: () {}),
                        SocialIcons(colorsList: [
                          Color(0xFFFF4F38),
                          Color(0xFFFF355D),
                        ], iconData: CustomIcons.googleplus, onPressed: () {}),
                        SocialIcons(colorsList: [
                          Color(0xFF17EAD9),
                          Color(0xFF6078EA),
                        ], iconData: CustomIcons.twitter, onPressed: () {}),
                        SocialIcons(colorsList: [
                          Color(0xFF00C6FB),
                          Color(0xFF005BEA),
                        ], iconData: CustomIcons.linkedin, onPressed: () {}),
                      ]),
                  SizedBox(
                    height: SizeConfig.safeBlockHorizontal * 4,
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
