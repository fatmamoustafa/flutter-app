import 'package:flutter/material.dart';

import '../helpers/size_config.dart';

class SocialIcons extends StatelessWidget {
  final List<Color> colorsList;
  final IconData iconData;
  final Function onPressed;

  SocialIcons({this.colorsList, this.iconData, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return new Padding(
      padding: EdgeInsets.only(left: SizeConfig.safeBlockVertical * 0.8),
      child: Container(
        width: SizeConfig.safeBlockVertical * 11,
        height: SizeConfig.safeBlockHorizontal * 11,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          gradient: LinearGradient(
            colors: colorsList,
            tileMode: TileMode.clamp,
          ),
        ),
        child: RawMaterialButton(
          shape: CircleBorder(),
          onPressed: onPressed,
          child: Icon(
            iconData,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
